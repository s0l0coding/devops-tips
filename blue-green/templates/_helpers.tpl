{{/*
Expand the name of the chart.
*/}}
{{- define "api-route-tester.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "api-route-tester.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "api-route-tester.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "api-route-tester.labels" -}}
helm.sh/chart: {{ include "api-route-tester.chart" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
# helm.sh/hook: pre-upgrade
# helm.sh/hook-delete-policy: before-hook-creation
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "api-route-tester.selectorLabels.green" -}}
app.kubernetes.io/name: {{ include "api-route-tester.name" . }}
# app.kubernetes.io/instance: {{ .Release.Name }}
app: {{ include "api-route-tester.fullname" .}}
version: {{ cat "v" .Values.deployment.green | nospace | replace "." ""}}
{{- end }}

{{- define "api-route-tester.selectorLabels.blue" -}}
app.kubernetes.io/name: {{ include "api-route-tester.name" . }}
# app.kubernetes.io/instance: {{ .Release.Name }}
app: {{ include "api-route-tester.fullname" .}}
version: {{ cat "v" .Values.deployment.blue | nospace | replace "." ""}}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "api-route-tester.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "api-route-tester.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}
